#!/bin/bash

PG='java Sudoku'
OPT='-test'
ARGS='420 009 500 000 000 097 970 068 002 000 690 308 090 253 010 503 084 000
100 830 069 780 000 000 004 900 071'

ARGS2='000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000
000 000 000 000 000 000 000 000 000'

ARGS3='999 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000 000
000 000 000 000 000 000 000 000 000'

if [[ $2 = '-all' ]] ; then
    PG='java -Xmx2048M SudokuAll'
fi

if [[ $1 = '-test' ]] ; then
    CMD="$PG $OPT $ARGS"
else if [[ $1 = '-empty' ]] ; then
    CMD="$PG $ARGS2"
else if [[ $1 = '-impossible' ]] ; then
    CMD="$PG $ARGS3"
else
    CMD="$PG $ARGS"
fi
fi
fi
$CMD
