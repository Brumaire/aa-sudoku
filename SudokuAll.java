/**
 * Solveur de grilles de Sudoku, utilisant un algorithme de recherche avec
 * retour arrière (backtracking), adapté pour sortir toutes les solutions,
 * quand elles existent.
 *
 * Réalisé dans le cadre d'un projet de travaux pratiques pour le DUT AS
 * Informatique de l'IUT Nancy-Charlemagne de L'Université de Lorraine.
 *
 * @author Lucas Vincent lucas.vincent1@etu.univ-lorraine.fr
 * @author Karim Rahmouni karim.rahmouni3@etu.univ-lorraine.fr
 **/
public class SudokuAll {

    ///////////////////////////////////////////////////////////////////////////
    // Variables de classe
    ///////////////////////////////////////////////////////////////////////////       
        /**
         * Booléen déterminant le comportement de sortie à utiliser.
         *
         * False : On affiche la grille d'entrée ainsi que la grille de sortie.
         * True  : On affiche une représentation de la grille sous forme de
         * triplets, comme l'entrée du programme.
         **/
        private static boolean sortieSimple = false;

        /**
         * Tableau d'entiers modélisant une grille de sudoku.
         **/
        private static int[] grille = new int[81];

        /**
         * Compteur de cases valuées
         **/
        private static int compteur = 0;

        /**
         * StringBuilder pour l'affichage
         **/
        private static StringBuilder affichage = new StringBuilder();

        /**
         * Compteur de concaténation pour vider le buffer et éviter
         * de saturer la RAM.
         **/
        private static int concatCompteur= 0;

        private static final int MAX_TAILLE_AFFICHAGE = 100;

    ///////////////////////////////////////////////////////////////////////////
    // Méthodes statiques auxiliaires
    ///////////////////////////////////////////////////////////////////////////

        /**
         * Méthode de traitement des options passées.
         * @param opt l'option à prendre en charge.
         **/
        private static void gérerOptions(String opt) {
            if(opt.startsWith("-")){
                switch(opt.substring(1,opt.length())){
                    case "test":
                        SudokuAll.sortieSimple = true;
                        break;
                    //Eventuelles autres options.
                    default:
                        System.err.println("[Erreur] Option non reconnue.");
                        System.err.println("Fin.");
                        System.exit(1);
                        break;
                }
            }
        } 

        /**
         * Méthode de traitement des arguments du programme.
         *
         * Doit assurer : 
         *  - Le respect du format pour les arguments.
         *  - La prise en charge des options possibles.
         *
         *  @param args Tableau des arguments.
         **/
        private static void gérerArguments(String[] args) {
            int pointeurGrille = 0;
            int valeurCase = 0;
            char c = '0';
            for( int i= 0; i < args.length; i++){
                //si c'est une option
                if( args[i].substring(0,1).equals("-")  ) {
                    SudokuAll.gérerOptions(args[i]);
                }
                // Si c'est un triplets d'entiers
                else {
                    try{
                        // Récupérer chaque caractère.
                        for( int j= 0; j < args[i].length(); j++) {
                            c = args[i].charAt(j);
                            // Tenter de parser un entier.
                            valeurCase = Integer.parseInt(""+c);

                            // On vérifie si la valeur passée comprise entre 0 et
                            // 9 (inclus).
                            if(valeurCase < 0 || valeurCase > 9)
                                throw new Exception();
                            else{
                                // Qu'elle n'est pas incompatible avec l'état
                                // précédent de la grille.
                                if(valeurCase != 0){
                                    if(0 != estLegal(valeurCase, pointeurGrille)){
                                        throw new Exception();
                                    }
                                }
                                // Si ces conditions sont remplies :
                                // On l'ajoute et on incrémente le compteur.
                                grille[pointeurGrille] = valeurCase;
                                pointeurGrille++;
                                if(valeurCase != 0) SudokuAll.compteur++;
                            }
                        }
                    }
                    // Blocs de gestion des exceptions.
                    catch(NumberFormatException e) {
                        System.err.println(
                            "[Erreur] Format des arguments "+
                            "du programme invalide.");
                        System.err.println("Fin.");
                        System.exit(1);
                    }
                    catch(ArrayIndexOutOfBoundsException e){
                        System.err.println(
                            "[Erreur] Les arguments comportent "+
                            " plus de 81 valeurs.");
                        System.err.println("Fin.");
                        System.exit(1);
                    }
                    catch(Exception e){
                        System.err.println(
                            "[Erreur] La valeur \""+ valeurCase + "\""+
                            " en position " + pointeurGrille +
                            " est illégale (incompatible avec la grille ou"+
                            " hors de l'intervalle autorisé). ");
                        System.err.println("Fin.");
                        System.exit(1);
                    }

                }
            }
        }

        /**
         * Méthode IO permettant d'afficher une grille de SudokuAll.
         **/
        private static void afficherGrille() {
            int pointeurGrille = 0;
            int chiffre = 0;

            String separateurHorizontal = " ----------------------- ";
            String separateurVertical = "|";

            StringBuilder sb = new StringBuilder(1300);

            //Parcours des lignes.
            for( int i= 0; i< 13; i++ ) {
                //Placer un séparateur vertical.
                if( i%4 == 0 ) {
                    sb.append(separateurHorizontal);
                }
                else {
                    // Parcours des colonnes (glyphes).
                    for( int j= 0; j< 25 ; j++ ) {
                        // Placer un espacement inter-sous-grilles.
                        if( j%8 == 0 ) {
                            sb.append(separateurVertical);
                        }
                        // Placer un espacement inter-caractères.
                        else if( j%2 != 0 ) {
                            sb.append(" ");
                        }
                        // Sinon récupérer et afficher la valeur numérique.
                        else {
                            chiffre = SudokuAll.grille[pointeurGrille];
                            if( chiffre != 0 )
                                sb.append(chiffre);
                            else
                                //cases vides.
                                sb.append(" ");
                            pointeurGrille++;
                        }
                    }
                }
                sb.append("\n");
            }
            SudokuAll.affichage.append(sb);
            SudokuAll.concatCompteur++;
            if( SudokuAll.concatCompteur == SudokuAll.MAX_TAILLE_AFFICHAGE){
                System.out.println(new String(SudokuAll.affichage));
                SudokuAll.affichage = new StringBuilder();
                SudokuAll.concatCompteur = 0;
            }
        }

        /**
         * Méthode IO permettant de représenter une grille de SudokuAll sous forme de
         * triplets d'entier.
         **/
        private static void afficherGrilleSimple() {
            StringBuilder sb = new StringBuilder(81 + 26);
            int pointeurGrille = 1;
            
            sb.append(SudokuAll.grille[0]);
            for( int i=1; i < 108; i++ ) {
                //un espace tous les 3 glyphes.
                if( i%4 == 3 )
                    sb.append(" ");
                else {
                    sb.append(SudokuAll.grille[pointeurGrille]);
                    pointeurGrille++;
                 }
            }
            SudokuAll.affichage.append(sb);
            SudokuAll.concatCompteur++;
            if( SudokuAll.concatCompteur == SudokuAll.MAX_TAILLE_AFFICHAGE){
                System.out.println(new String(SudokuAll.affichage));
                SudokuAll.affichage = new StringBuilder();
                SudokuAll.concatCompteur = 0;
            }
        }

        /**
         * Méthode retournant 0 si on peut placer un chiffre sur une case
         * donnée, sinon une somme correspondant aux collisions existantes sinon.
         *
         * 100 : Collision au niveau de la ligne.
         * 010 : Collision au niveau d'une colonne.
         * 001 : Collision au niveau d'une sous-grille.
         *
         * @param valeur valeur à tester pour la position donnée.
         * @param position position à tester.
         **/
        public static int estLegal(int valeur, int position){
            int result = 0;
            int pointeurGrille = 0;

        ////////////////////////////////////////////////////////////////////////
        // Variable utiles pour trouver le coin haut-gauche des sous-grilles.
        ////////////////////////////////////////////////////////////////////////
            int line;
            int col;
            int a;
            int b;   
        ////////////////////////////////////////////////////////////////////////


            // Même chiffre sur ligne ?
            pointeurGrille = position - position % 9;
            for(int i=0; i < 9; i++) {
                if(SudokuAll.grille[pointeurGrille+i] == valeur)
                    result += 100;
            }

            // Même chiffre sur colonne ?
            pointeurGrille = position - (9 * (position / 9));
            for(int j=0; j<9; j++) {
                if(SudokuAll.grille[pointeurGrille+(j*9)] == valeur)
                    result += 10;
            }
            

           // Même chiffre dans petit carré ?
               
         //////////////////////////////////////////////////////////////////////
         // 
         // Calcul permettant de retrouver le coin haut-gauche d'une
         // sous-grille à partir d'une case quelconque de cette sous-grille
         // pour la parcourir et tester les collisions de valeurs.
         //
         // Trouvé en tatonnant, il existe sans doute quelque chose de plus
         // simple ou efficace pour arriver au même résultat.
         //
         //////////////////////////////////////////////////////////////////////
                a = position - ( position % 3 );
                col = a - 9 * ( a / 9 );
                b = a - col;
                line = b - ( b % 27 );
                pointeurGrille = col + line;
         //////////////////////////////////////////////////////////////////////
        
            for(int k = 0; k<3; k++) {
                for(int l = 0; l<3; l++) {
                    if(SudokuAll.grille[pointeurGrille+(k*9)+l] == valeur)
                        result += 1;    
                }
            }
            
            return result;
        }

        /**
         * Méthode récursive de remplissage de la grille.
         **/
        private static boolean remplirGrille(){
            return remplirGrille(0,0);
        }

        /**
         * Méthode récursive de remplissage de la grille, paramétrée.
         *
         * @param position position de la grillle à partir de laquelle on
         * commence le remplissage.
         *
         * @param val valeur à partir de laquelle on commence le remplissage.
         **/
        private static boolean remplirGrille(int position, int val) {
            boolean solution = false;
            int iteration = 1;
            int légalité = 0;
            int essai = 0;
            if(SudokuAll.compteur == 81){
                if(SudokuAll.sortieSimple)
                    SudokuAll.afficherGrilleSimple();
                else {
                    SudokuAll.afficherGrille();
                }
            }
            else{
                // Tant qu'on a pas rempli la grille OU testé toutes les
                // possibilités.
                while(position < SudokuAll.grille.length && !solution){
                    //Si la case est vide
                    if(grille[position] == 0){ 

                        // tester chaque valeur.
                        while(iteration <= 9 && !solution){
                             essai = (iteration+val)%10;
                            légalité = estLegal(essai, position);
                                
                            // Si la position est légale, on place.
                            if(légalité == 0){   
                                SudokuAll.grille[position] = essai;
                                SudokuAll.compteur++;
                                // et on exécute un appel récursive à la
                                // position n+1 et valeur n+1.
                                 solution = 
                                    remplirGrille( (position+1)%81, essai);
                            
                                // Si notre engagement dans l'arbre de décision
                                // ne permet pas de trouver une solution
                                // acceptable --> retour-arrière.
                                  if(!solution){
                                    SudokuAll.compteur--;
                                    SudokuAll.grille[position] = 0;
                                    }
                                }
                            iteration++;
                        } // Fin while.
                    }
                    position++;
                } // Fin while 2.

            }// Fin else.
            return solution;
        }

    ///////////////////////////////////////////////////////////////////////////
    // Méthode exécutable
    ///////////////////////////////////////////////////////////////////////////
        public static void main(String[] args) {
            boolean solution = false;

            SudokuAll.gérerArguments(args);

            //Affichage pré-résolution.
            if(!SudokuAll.sortieSimple)
                SudokuAll.afficherGrille();
            
            SudokuAll.remplirGrille();
            System.out.println(new String(SudokuAll.affichage));
        }
}
